var flo = require('fb-flo'),
    path = require('path'),
    fs = require('fs'),
    exec = require('child_process').exec;

var server = flo(
  './source',
  {
    port: 8888,
    host: 'localhost',
    verbose: false,
    glob: [
      '**/*.js',
      '**/*.css'
    ]
  },
  function resolver(filepath, callback) {
    exec('npm run build', function(error) {
      if (error) throw error;
      callback({
        resourceURL: 'build/index.min' + path.extname(filepath),
        // any string-ish value is acceptable. i.e. strings, Buffers etc.
        contents: fs.readFileSync('build/index.min' + path.extname(filepath)).toString(),
        reload: true,
        update: function(_window, _resourceURL) {
          console.log("Resource " + _resourceURL + " has just been updated with new content");
        }
      });
    });
  }
);
console.log('flo is running on localhost:8888');
